from scipy.optimize import curve_fit
from scipy.stats import poisson
import matplotlib.pyplot as plt
import numpy as np


def fit(
    numbers: np.ndarray,
    max_value=None,
    xlabel="Initial number of cells",
    title="",
    plot=True,
    save_fig_path=None,
):
    if max_value is None:
        max_value = numbers.max()
    bins = np.arange(max_value + 1) - 0.5
    vector = bins[:-1] + 0.5
    hist, bins = np.histogram(numbers, bins=bins, density=True)
    popt, pcov = curve_fit(poisson.pmf, vector, hist, p0=(1.0,))
    lambda_fit_result = popt[0]
    if plot:
        plt.hist(numbers, bins=bins, fill=None)
        plt.plot(
            vector,
            len(numbers) * poisson.pmf(vector, lambda_fit_result),
            ".-",
            label=f"Poisson fit λ={lambda_fit_result:.1f}",
            color="tab:red",
        )
        plt.xlabel(xlabel)
        plt.title(title)
        plt.legend()
        if save_fig_path is not None:
            try:
                plt.savefig(save_fig_path)
                print(f"Save histogram {save_fig_path}")
            except Exception as e:
                print("saving histogram failed", e.args)
        plt.close()
    return lambda_fit_result
