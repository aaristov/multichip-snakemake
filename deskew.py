import fire
import numpy as np
import dask.array as da
from zarr_tools import convert

def deskew(a:np.ndarray, step:int=-1) -> np.ndarray:
    b = np.zeros_like(a)
    for row in range(a.shape[-2]):
        b[...,row,:] = np.roll(a[...,row,:], row*step, axis=-1)
    return b

def main(input_zarr:str, out_zarr:str):
    data = da.from_zarr(input_zarr + '/0/')
    chunks = np.ones_like(data.shape)
    chunks[-2:] = data.shape[-2:]
    data.rechunk(tuple(chunks))
    new = data.map_blocks(deskew)
    convert.to_zarr(new, out_zarr, channel_axis=None, steps=4)
    return out_zarr


if __name__ == "__main__":
    fire.Fire(main)