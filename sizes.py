import nd2
import sys
import fire

def sizes(path:str):
	return {"sizes": nd2.ND2File(path).sizes, "path": path}

if __name__ == '__main__':
	fire.Fire(sizes)
