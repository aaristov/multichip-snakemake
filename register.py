from tifffile import imread
import matplotlib.pyplot as plt
import imreg_dft as reg
import numpy as np


def align_stack(
    data_or_path,
    template16,
    mask2,
    plot=False,
    binnings=(1, 16, 2),
):
    """
    stack should contain two channels: bright field and fluorescence.
    BF will be binned 8 times and registered with template8 (aligned BF).
    When the transformation verctor will be applied to the original data and
    stacked with the mask.
    The output stack is of the same size as mask.

    :return: aligned_array[bf, fluo, mask], tvec
    :rtype: np.ndarray, dict

    """
    if isinstance(data_or_path, str):
        path = data_or_path
        stack = imread(path)
        print(path, stack.shape)
    else:
        assert data_or_path.ndim == 3 and data_or_path.shape[0] == 2
        stack = data_or_path

    bf, tritc = stack[:2]
    stack_temp_scale = binnings[1] // binnings[0]
    mask_temp_scale = binnings[1] // binnings[2]
    stack_mask_scale = binnings[2] // binnings[0]

    f_bf = bf[::stack_temp_scale, ::stack_temp_scale]

    tvec8 = get_transform(f_bf, template16, plot=plot)
    plt.show()
    tvec = scale_tvec(tvec8, mask_temp_scale)
    print(tvec)
    try:
        aligned_tritc = unpad(
            transform(tritc[::stack_mask_scale, ::stack_mask_scale], tvec),
            mask2.shape,
        )
        aligned_bf = unpad(
            transform(bf[::stack_mask_scale, ::stack_mask_scale], tvec),
            mask2.shape,
        )
    except ValueError as e:
        print("stack_mask_scale: ", stack_mask_scale)
        print(e.args)
        raise e

    aligned_stack = np.stack((aligned_bf, aligned_tritc, mask2)).astype(
        "uint16"
    )

    return aligned_stack, tvec


def get_transform(
    image, template, plot=True, pad_ratio=1.2, figsize=(10, 5), dpi=300
):
    """
    Pads image and template, registers and returns tvec
    """
    padded_template = pad(template, (s := increase(image.shape, pad_ratio)))
    padded_image = pad(image, s)
    tvec = register(padded_image, padded_template)
    if plot:
        aligned_bf = unpad(tvec["timg"], template.shape)
        plt.figure(figsize=figsize, dpi=dpi)
        plt.imshow(aligned_bf, cmap="gray")
    return tvec


def register(image, template):
    """
    Register image towards template
    Return:
    tvec:dict
    """
    assert np.array_equal(
        image.shape, template.shape
    ), f"unequal shapes {(image.shape, template.shape)}"
    return reg.similarity(
        template,
        image,
        constraints={
            "scale": [1, 0.2],
            "tx": [0, 50],
            "ty": [0, 50],
            "angle": [0, 30],
        },
    )


def filter_by_fft(
    image,
    sigma=40,
    fix_horizontal_stripes=False,
    fix_vertical_stripes=False,
    highpass=True,
):
    size = get_fft_size(image.shape)
    fft = np.fft.fft2(pad(image, size))
    if fix_vertical_stripes:
        fft[0, 1:] = 0  # vertical
    if fix_horizontal_stripes:
        fft[1:, 0] = 0  # horizontal
    fft = fft * (fft_mask(size, sigma=size[0] / sigma, highpass=highpass))
    filtered_image = unpad(abs(np.fft.ifft2(fft)), image.shape)
    return filtered_image


def fft_mask(shape, sigma=10, highpass=False):
    y, x = np.indices(shape)

    gaus = np.exp(-((x - shape[1] // 2) ** 2) / sigma**2 / 2) * np.exp(
        -((y - shape[0] // 2) ** 2) / sigma**2 / 2
    )
    gaus = gaus / gaus.max()
    if highpass:
        return 1 - gaus
    return gaus


def pad(image: np.ndarray, to_shape: tuple = None, padding: tuple = None):
    if padding is None:
        padding = calculate_padding(image.shape, to_shape)
    try:
        padded = np.pad(image, padding, "edge")
    except TypeError as e:
        print(e.args, padding)
        raise e
    return padded


def unpad(image: np.ndarray, to_shape: tuple = None, padding: tuple = None):
    if any(np.array(image.shape) - np.array(to_shape) < 0):
        print(
            f"""unpad:warning: image.shape {image.shape} \
            is within to_shape {to_shape}"""
        )
        image = pad(image, np.array((image.shape, to_shape)).max(axis=0))
        print(f"new image shape after padding {image.shape}")
    if padding is None:
        padding = calculate_padding(to_shape, image.shape)

    y = [padding[0][0], -padding[0][1]]
    if y[1] == 0:
        y[1] = None
    x = [padding[1][0], -padding[1][1]]
    if x[1] == 0:
        x[1] = None
    return image[y[0]: y[1], x[0]: x[1]]


def get_fft_size(shape):
    max_size = np.max(shape)
    n = 5
    while (2**n) < (max_size * 1.5):
        n += 1
    return (2**n, 2**n)


def calculate_padding(shape1: tuple, shape2: tuple):
    """
    Calculates padding to get shape2 from shape1
    Return:
    2D tuple of indices
    """
    dif = np.array(shape2) - np.array(shape1)
    assert all(
        dif >= 0
    ), f"Shape2 must be bigger than shape1, got {shape2}, {shape1}"
    mid = dif // 2
    rest = dif - mid
    y = mid[0], rest[0]
    x = mid[1], rest[1]
    return y, x


def scale_tvec(tvec, scale=8):
    tvec_8x = tvec.copy()
    tvec_8x["tvec"] = tvec["tvec"] * scale
    try:
        tvec_8x["timg"] = None
    except KeyError:
        pass
    finally:
        return tvec_8x


def transform(image, tvec):
    print(f"transform {image.shape}")
    fluo = reg.transform_img_dict(image, tvec)
    return fluo.astype("uint")


def increase(shape, increase_ratio):
    assert increase_ratio > 1
    shape = np.array(shape)
    return tuple((shape * increase_ratio).astype(int))
