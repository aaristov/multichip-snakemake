rule combine_tables:
	input:
		table1="{folder}/day1/table.csv",
		table2="{folder}/day2/table.csv",
		crops = "{folder}/day2/BF_TRITC_aligned.crops.zarr"
	params:
		threshold="20" #minimum number of cells for day 2

	output:
		table="{folder}/final_table.csv",
		swarm_plot="{folder}/swarm_plot.png",
		swarm_plot_int="{folder}/swarm_plot_intensities.png",
		prob_plot="{folder}/prob_plot.png",
		prob_plot_log="{folder}/prob_plot_log.png"
	shell:
		"python merge_tables.py {input.table1} {input.table2} {output.table} {output.swarm_plot} {output.prob_plot} {params.threshold}"

rule align_and_count:
	input:
		data="{folder}/day{day}/BF_TRITC_maxZ.zarr",
		concentrations="{folder}/concentrations.yaml",
		template="v3/template_bin16_v3.tif",
		labels="v3/labels_bin2_v4.tif"
	output:
		zarr = directory("{folder}/day{day}/BF_TRITC_aligned.zarr"),
		table = "{folder}/day{day}/table.csv"
	shell:
		"python align.py {input.data} {output.zarr} {input.concentrations} {input.template} {input.labels} {output.table} 1" 

rule make_crops:
	input:
		data = "{folder}/day{day}/BF_TRITC_aligned.zarr"
	output:
		zarr = directory("{folder}/day{day}/BF_TRITC_aligned.crops.zarr")
	shell:
		"python make_crops.py {input.data} {output.zarr}"


rule correct_xy:
	input:
		zarr = "{folder}/day{day}/BF_TRITC_aligned-to-correct.zarr",
		table = "{folder}/day{day}/correction.csv"
	output:
		zarr = directory("{folder}/day{day}/BF_TRITC_aligned-corrected.zarr")
	shell:
		"python correct.py {input.zarr} {input.table} {output.zarr}"

rule align_and_count_2D:
	input:
		data="{folder}/day{day}/BF-TRITC-2D.zarr",
		concentrations="{folder}/concentrations.yaml",
		template="v3/template_bin16_v3.tif",
		labels="v3/labels_bin2_v4.tif"
	output:
		zarr = directory("{folder}/day{day}/BF_TRITC_aligned.zarr"),
		table = "{folder}/day{day}/table.csv"
	shell:
		"python align.py {input.data} {output.zarr} {input.concentrations} {input.template} {input.labels} {output.table} 0" 
	

rule get_sizes_nd2:
	input:
		"{file}.nd2"
	output:
		"{file}-sizes.txt"
	shell:
		"python sizes.py {input} > {output}"

rule combine_BF_TRITC_3D_maxZ:
	input: 
		bf="{folder}/day{day}/BF-2D.nd2",
		fluo="{folder}/day{day}/TRITC-3D.nd2"
	output:
		directory("{folder}/day{day}/BF_TRITC_maxZ.zarr")
	shell:
		"python combine.py {input.bf} {input.fluo} {output}" 


rule convert_nd2_zarr:
	input:
		"{file}D.nd2"
	output:
		directory("{file}D.zarr")
	shell:
		"python -m zarr_tools {input}"


# rule deskew:
# 	input:
# 		"{zarr}-skewed.zarr"
# 	output:
# 		directory("{zarr}.zarr")
# 	shell:
# 		"python deskew.py {input} {output}"