import dask.array as da
import os
from zarr_tools.convert import to_zarr
import fire
import nd2

def combine(BF_2D_path:str, TRITC_3D_path:str, out_zarr_path:str):
    bd2d = nd2.ND2File(BF_2D_path).to_dask()
    print('Opened BF:', bd2d)
    fd3d = nd2.ND2File(TRITC_3D_path).to_dask()
    print('Opened TRITC:', fd3d)
    fd2d = fd3d.max(axis=1)
    bd2d = da.stack([bd2d, fd2d], axis=1)
    print('Resulting stack:', bd2d)
    to_zarr(
        bd2d, 
        path=out_zarr_path, 
        steps=4, 
        name=['BF','TRITC'], 
        colormap=['gray','green'],
        lut=((1000,30000),(440, 600)),
    )

# combine(snakemake.input[0], snakemake.input[1], snakemake.output[0])
if __name__ == "__main__":
    fire.Fire(combine)