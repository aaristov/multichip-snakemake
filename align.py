import numpy as np
import register
import poisson
from skimage.measure import regionprops, regionprops_table
import count
import tifffile as tf
import os
import dask.array as da
from functools import partial
from zarr_tools import convert
from zarr.errors import ArrayNotFoundError
import yaml
import fire
from multiprocessing import Pool
import nd2
import pandas as pd
from scipy.ndimage import laplace, rotate
import json



def align_multichip(
    BF_TRITC_2D_path,
    out_path,
    concentrations_path,
    template_path,
    labels_path,
    table_path,
    fit_poisson,
    nmax=10,
):
    with open(concentrations_path, "r") as f:
        concentrations_dct = yaml.safe_load(f)
    concentrations = concentrations_dct["concentrations"]
    unit = concentrations_dct["units"]
    print(
        f"concentrations from {concentrations_path}: {concentrations} [{unit}]"
    )

    if "rotation_data_deg" in concentrations_dct:
        rotation_data_deg = int(concentrations_dct["rotation_data_deg"])
        print(f"Rotation: {rotation_data_deg}")
    else:
        rotation_data_deg = 0

    data = read_dask(BF_TRITC_2D_path)
    template16 = tf.imread(template_path)
    big_labels = tf.imread(labels_path)

    fun = partial(
        align_parallel,
        template16=template16,
        big_labels=big_labels,
        unit=unit,
        fit_poisson=fit_poisson,
        nmax=nmax,
        rotation_data_deg=rotation_data_deg,
        aligned_path=out_path
    )

    try:
        p = Pool(data.shape[0])
        out = p.map(fun, zip(data, range(len(concentrations)), concentrations))

    except TypeError as e:
        print(f"Pool failed due to {e.args}")
        out = list(map(fun, zip(data, concentrations)))
    finally:
        p.close()

    aligned = [o["stack"] for o in out]
    counts = [o["counts"] for o in out]
    tvecs = [o["tvec"] for o in out]

    save_tvecs(tvecs, out_path.replace(".zarr", ".transform.json"))

    df = pd.concat(counts, ignore_index=True).sort_values(["[AB]", "label"])
    df.to_csv(table_path)

    daligned = da.from_array(np.array(aligned))
    convert.to_zarr(
        daligned,
        path=out_path,
        steps=4,
        name=["BF", "TRITC", "mask"],
        colormap=["gray", "green", "blue"],
        lut=((1000, 30000), (440, 600), (0, 501)),
    )
    return out_path


def prep_tvec(transform_dict):
    t = transform_dict.copy()
    t["tvec"] = list(t["tvec"])
    return t


def save_tvecs(tvecs, path):
    try:
        transform_data = list(map(prep_tvec, tvecs))
        with open(path, "w") as f:
            json.dump(transform_data, fp=f)
    except Exception as e:
        print("saving transform json failed: ", e.args)


def align_parallel(args, **kwargs):
    return align2D(*args, **kwargs)


def align2D(
    stack_dask,
    chip_index,
    ab,
    template16=None,
    big_labels=None,
    unit="μg_mL",
    fit_poisson=True,
    nmax=20,
    rotation_data_deg=0,
    aligned_path=""
):
    print(ab, unit)
    try:
        aligned = da.from_zarr(os.path.join(aligned_path, "0"))[chip_index].compute()
        print(f"already aligned: {aligned_path}:{aligned.shape}")
        counts = count_cells(aligned, ab=ab)
        intensity_table = get_intensity_table(aligned[2], aligned[1])
        counts.loc[:, "intensity"] = intensity_table.intensity
        return {"stack": aligned, "counts": counts}
    except ArrayNotFoundError:
        print("Processing...")

    data = stack_dask.compute()
    if rotation_data_deg != 0:
        data = rotate(input=data, angle=rotation_data_deg, axes=(1, 2))
        print(f"Rotated data {rotation_data_deg} deg")

    aligned, tvec = register.align_stack(
        data,
        template16=template16,
        mask2=big_labels,
        binnings=(2, 16, 2),
    )
    counts = count_cells(aligned, ab=ab)
    intensity_table = get_intensity_table(aligned[2], aligned[1])
    counts.loc[:, "intensity"] = intensity_table.intensity

    if fit_poisson:
        lambda_fit_result = poisson.fit(
            counts.query(f"n_cells < {nmax}").n_cells,
            title=f"automatic {ab}{unit.replace('_','/')}",
            save_fig_path=aligned_path.replace(".zarr", f"{ab}-counts-hist.png"),
        )
        counts.loc[:, "poisson fit"] = lambda_fit_result

    return {"stack": aligned, "counts": counts, "tvec": tvec}


def count_cells(aligned: np.ndarray, ab=None):
    counts = get_cell_numbers(
        aligned[1], aligned[2], threshold_abs=2, plot=False, bf=aligned[0]
    )
    counts.loc[:, "[AB]"] = ab
    return counts


def get_cell_numbers(
    multiwell_image: np.ndarray,
    labels: np.ndarray,
    plot=False,
    threshold_abs: float = 2,
    min_distance: float = 5,
    meta: dict = {},
    bf: np.ndarray = None,
) -> pd.DataFrame:
    props = regionprops(labels)

    def get_n_peaks(i):
        if bf is None:
            return count.get_peak_number(
                multiwell_image[props[i].slice],
                plot=plot,
                dif_gauss_sigma=(3, 5),
                threshold_abs=threshold_abs,
                min_distance=min_distance,
                title=props[i].label,
            )
        else:
            return count.get_peak_number(
                multiwell_image[props[i].slice],
                plot=plot,
                dif_gauss_sigma=(3, 5),
                threshold_abs=threshold_abs,
                min_distance=min_distance,
                title=props[i].label,
                bf_crop=bf[props[i].slice],
                return_std=True,
            )

    n_cells = list(map(get_n_peaks, range(labels.max())))
    return pd.DataFrame(
        [
            {
                "label": prop.label,
                "x": prop.centroid[0],
                "y": prop.centroid[1],
                "n_cells": n_cell[0],
                # 'std': n_cell[1],
                **meta,
            }
            for prop, n_cell in zip(props, n_cells)
        ]
    )


def get_intensity_table(
    labelled_mask: np.ndarray,
    intensity_image: np.ndarray,
    values=[
        "mean_intensity",
    ],
):
    assert (
        iis := intensity_image
    ).ndim == 2, f"expected 2D array for intensity, got shape {iis.shape}"
    data = intensity_image.astype("f")
    dict_li = regionprops_table(
        labelled_mask, intensity_image=data, properties=["label", *values]
    )
    dict_bg = regionprops_table(
        get_outlines(labelled_mask),
        intensity_image=data,
        properties=["mean_intensity"],
    )
    dict_litb = {**dict_li, "bg_mean": dict_bg["mean_intensity"]}
    df = pd.DataFrame.from_dict(dict_litb)

    df.loc[:, "intensity"] = df.mean_intensity - df.bg_mean
    return df


def get_outlines(labels):
    """creates 1 px outline around labels"""
    return labels * (np.abs(laplace(labels)) > 0)


def read_dask(path: str):
    if path.endswith(".zarr"):
        data = da.from_zarr(path + "/0/")
    elif path.endswith(".nd2"):
        data = nd2.ND2File(path).to_dask()
    else:
        raise ValueError("Unexpected file format, expected zarr or nd2")
    print("data:", data)
    return data


if __name__ == "__main__":
    fire.Fire(align_multichip)
