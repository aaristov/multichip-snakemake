# Multichip-snakemake

This a snakemake pipeline allowing to reconstruct the 6-chips data automatically

## Installation

git clone https://gitlab.pasteur.fr/aaristov/multichip-snakemake.git
cd multichip-snakemake
pip install -r requirements.txt

## Running locally

On Athena computer find and launch the icon on the desktop "Snakemake".
The terminal will open.
The command to launch reconstruction:
```
snakemake -c1 D:\Andrey\Data\path_to_final_table.csv
```
For conveniense it is possible to drag and drop the folder into the terminal to get a valid path, then simply add the missing filename at the end.

## Running on the cluster

```
ssh USERNAME@maestro.pasteur.fr
snk
snakemake --cluster "srun -c12" -j32 ~/Anchor/Lena/Data/20220531-MIC-e.coli-cipro/{1st,2nd}exp/final_table.csv --cores 12
```
In another terminal you can see the status of the jobs using *sacct*
``` 
watch sacct
```
